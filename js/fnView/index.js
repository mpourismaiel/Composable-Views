import view from './view'

let compView = {}

export default function (options = {}, ...children) {
  const v = Object.assign({}, compView, view)
  v.template = options.template
  return v.view(options.data, children)
}
