import Mustache from 'mustache'

export default {
  getTemplate () {
    if (typeof this.template === 'string') {
      return this.template
    } else if (typeof this.template === 'function') {
      return this.template()
    } else {
      console.log('Error: Template must be either a function or a string.', this)
    }
  },

  view (data = {}, children = []) {
    return () => {
      let renderedChildren = []

      children.forEach((child, i) => {
        renderedChildren[i] = child()
      })

      return this.render(data, renderedChildren)
    }
  },

  render (data, children) {
    const concatChildren = children.join('')

    const rendered = this.getTemplate().replace('{{%s}}', concatChildren)

    return Mustache.render(rendered, data)
  }
}
