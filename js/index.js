import view from './fnView/index'

const Item = view({
  template: '<h1>{{ title }}</h1>',
  data: {title: 'Hello World'}
})

const Item2 = view({
  template: '<h2>{{ title }}</h2>',
  data: {title: 'This is composed!'}
})

const Container = view({
  template: '<div>{{%s}}</div>',
  data: {}
}, Item, Item2)

console.log(Container())
