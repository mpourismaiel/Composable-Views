Views should be composable. That's just the way they should be! The one true
way!

This module helps build composable views more easily. Just import this and try
something like this:

```javascript
import view from './fnView/index'

const Label = view({
  template: '<label for="{{forInput}}">{{name}}</label>',
  data: {
    forInput: 'name',
    name: 'Name'
  }
})

const Input = view({
  template: '<input type="text" name="{{name}}">',
  data: {
    name: 'name'
  }
})

const Form = view({
  template: '<form action="{{action}}">{{%s}}</form>',
  data: {
    action: '/'
  }
}, Label, Input)
```

The template engine is *Mustache*, I'll replace it with *JSX* for a better
readability. Also these views are completely static. They wouldn't be in the
future!
